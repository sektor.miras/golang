FROM golang

COPY . /golang

WORKDIR /golang

RUN go mod download

ENV GO111MODULE=on\
    CGO_ENABLED=0\
    GOOS=linux\
    GOARCH=amd64
RUN go build /golang/cmd/web

ENTRYPOINT ["./web"]

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=0 /golang .

CMD ["./web"]

EXPOSE 4000